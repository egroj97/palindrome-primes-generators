require 'prime'
require 'time'
require 'benchmark'

class String
  def palindrome?

    if self == self.reverse
      return true
    else
      return false
    end

  end
end

def palprime_generator(n)

  (1..Float::INFINITY).lazy.select do |val|
    val.to_s.palindrome? and val.prime?
  end.first(n)

end

def main()
  print "Quantity of numbers to be generated: "
  n = gets.to_i
  
  t1 = Time.now
  arr = palprime_generator(n)
  t2 = Time.now

  print arr
  puts "\nExecution time: #{t2-t1} seconds"  
end

#print numb_select(gets.to_i)

main()
