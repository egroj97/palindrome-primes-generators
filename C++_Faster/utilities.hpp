//The noexcept feature works as for the program content

# include <iostream>
# include <string>
# include <chrono>
# include <vector>

using namespace std;
using namespace std::chrono;
using ULL = unsigned long long;

constexpr ULL sqrt_of(ULL &) noexcept;
ULL * palprime_generator(ULL &, ULL *) noexcept;
bool is_palindrome(string &&) noexcept;
bool is_prime(ULL &) noexcept;
void print_array(ULL *, ULL &) noexcept;
