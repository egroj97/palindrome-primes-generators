# include <utilities.hpp>
# include <cmath>

constexpr ULL sqrt_of(ULL & n) noexcept
{
  return sqrt(n);
}

//Palindrome Prime Numbers Generators
ULL * palprime_generator(ULL & n, ULL * vect) noexcept
{
  ULL i {3};
  ULL count {0};

  if (n >= 1)
    {
      vect[0] = 2;
      count++;
    }
  
  while (count != n)
    {
      if (is_prime(i) and is_palindrome(to_string(i)))
	{
	  vect[count] = i;
	  count++;
	}
      i+=2;
    }

  return vect;
}

//Palindrome checker
bool is_palindrome(string && str) noexcept
 {
   size_t i {0};
   size_t j {str.length()-1};

   while (j > i)
     {
       if (str[i] != str[j])
	 return false;

       i++;
       j--;
     }

   return true;
 }

//Prime checker
bool is_prime(ULL & n) noexcept
{
  ULL i;

  for (i = 3;i <= sqrt_of(n);i = i + 2)
    if (n % i == 0)
      return false;

  return true;
}

void print_array(ULL * ptr, ULL & n) noexcept
{
  cout << '[';
	  
  for(size_t i = 0;i < n-1 ;i++)
    cout << ptr[i] << ", ";
		       
  cout << ptr[n-1] << "]\n";

  return;
}
