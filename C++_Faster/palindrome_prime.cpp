/**
   @author Jorge Luis Rodríguez González
   ///Made in C++14
**/

# include <utilities.hpp>

using hrc_time_point = high_resolution_clock::time_point;
using duration_db = duration<double>;

int main(int argc, char ** argv)
{
  cout << "Quantity of numbers to be generated: ";
  ULL n;
  cin >> n;

  ULL arr[n];
  
  hrc_time_point t1 = high_resolution_clock::now();

  palprime_generator(n,arr);
  
  hrc_time_point t2 = high_resolution_clock::now();
  
  duration_db exec_time = duration_cast<duration_db>(t2 - t1);

  print_array(arr, n);
    
  cout << "Execution time: "
    << static_cast<long double>(exec_time.count())
       << " Seconds" << endl;
  
  return 0;
}

