/**
   @author Jorge Luis Rodríguez González
   ///Made in C++14
**/

# include <utilities.hpp>

int main(int argc, char ** argv)
{
  cout << "Quantity of numbers to be generated: ";
  ULL n;
  cin >> n;
  
  hrc_time_point t1 = high_resolution_clock::now();

  vector_ull * ptr {palprime_generator(n)};
  
  hrc_time_point t2 = high_resolution_clock::now();
  
  duration_db exec_time = duration_cast<duration_db>(t2 - t1);

  cout << *ptr;
    
  cout << "Execution time: "
    << static_cast<long double>(exec_time.count())
       << " Seconds" << endl;
  
  return 0;
}

