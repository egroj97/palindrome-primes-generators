//The noexcept feature works as for the program context
//The is_prime function only works for this implementation

# include <iostream>
# include <string>
# include <chrono>
# include <vector>

using namespace std;
using namespace std::chrono;
using ULL = unsigned long long;
using vector_ull = vector<ULL>;
using hrc_time_point = high_resolution_clock::time_point;
using duration_db = duration<double>;

vector_ull * palprime_generator(ULL &) noexcept;
bool is_palindrome(string &&) noexcept;
bool is_prime(ULL &) noexcept;
ostream & operator<<(ostream & os, vector_ull &) noexcept;
