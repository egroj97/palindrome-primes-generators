# include <utilities.hpp>

//Palindrome Prime Numbers Generators
vector_ull * palprime_generator(ULL & n) noexcept
{
  ULL i {3};
  ULL count {0};  
  vector_ull * ptr {new vector_ull(n)};
  
  if (n >= 1)
    {
      ptr->at(0) = 2;
      count++;
    }
  
  while (count != n)
    {
      if (is_prime(i) and is_palindrome(to_string(i)))
	{
	  ptr->at(count) = i;
	  count++;
	}
      i+=2;
    }

  return ptr;
}

//Palindrome checker
bool is_palindrome(string && str) noexcept
 {
   size_t i {0};
   size_t j {str.length()-1};

   while (j > i)
     {
       if (str[i] != str[j])
	 return false;

       i++;
       j--;
     }

   return true;
 }

//Prime checker
bool is_prime(ULL & n) noexcept
{
  ULL i;

  for (i = 3;i < (n/2 + 1);i = i + 2)
    if (n % i == 0)
      return false;

  return true;
}

//Overload of the ostream operator
ostream & operator<<(ostream & os, vector_ull & ptr) noexcept
{
  cout << '[';
  for(auto item : ptr)
    if(item != ptr.back())
      cout << item << ", ";

  cout << ptr.back() << "]\n";
}
